//illustration
import welcomeAuth from './illustration/vehicle.png';
import IllustrationRegister from './illustration/register.svg';
import IllustrationLogin from './illustration/login.svg';
//Icon
import IconBack from './icon/back.svg';

export {welcomeAuth, IllustrationRegister, IllustrationLogin, IconBack};