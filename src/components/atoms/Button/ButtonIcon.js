import React from 'react'
import { StyleSheet } from 'react-native'
import {IconBack} from '../../../assets';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ButtonIcon = ({...rest}) => {
  return (
    <TouchableOpacity {...rest}>
      {rest.name === 'back' && <IconBack style={styles.iconBack}/>}
    </TouchableOpacity>
  )
}

export default ButtonIcon

const styles = StyleSheet.create({
  iconBack: {
    width: 25,
    height: 25
  },
})
