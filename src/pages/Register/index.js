import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Input, Button } from '../../components';
import { colors } from '../../utils';
import { IllustrationRegister } from '../../assets';
import { useSelector, useDispatch } from 'react-redux';
import { setForm } from '../../redux';

const Register = ({navigation}) => {
  const {form} = useSelector(state => state.RegisterReducer);
  const dispatch = useDispatch();
  
  const sendData = () => {
    console.log('data yang dikirim: ', form);
  };

  const onInputChange = (value, inputType) => {
    dispatch(setForm(inputType, value));
  }
  return(
    <View style={styles.wrapper.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
      <Button type="icon" name="back" onPress={() => navigation.goBack()} />
        <IllustrationRegister style={styles.illustration} />
        <Text style={styles.text.desc}>
          Mohon mengisi beberapa data untuk proses daftar anda 
        </Text>
        <View style={styles.space(64)} />
        <Input 
          placeholder="nama lengkap" 
          value={form.fullName} 
          onChangeText={(value) => onInputChange(value, 'fullName')}
        />
        <View style={styles.space(33)} />
        <Input 
          placeholder="email" 
          value={form.email} 
          onChangeText={(value) => onInputChange(value, 'email')}
        />
        <View style={styles.space(33)} />
        <Input 
          placeholder="password" 
          value={form.password}
          onChangeText={(value) => onInputChange(value, 'password')}
          secureTextEntry={true}
        />
        <View style={styles.space(83)} />
        <Button title="Daftar" onPress={sendData}/>
      </ScrollView>
    </View>
  );
}

const styles = {
  wrapper: {
    page: {padding: 20}
  },
  iconBack: {
    width: 25,
    height: 25
  },
  illustration: {
    width: 106, 
    height: 115, 
    marginTop: 8
    },
  text: {
    desc: {
      fontSize: 14, 
      fontWeight: 'bold', 
      color: colors.default, 
      marginTop: 16, 
      maxWidth: 200
    }
  },
  space: (value) => {
    return {
      height: value
    }
  }
}

export default Register;